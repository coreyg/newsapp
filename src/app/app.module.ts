import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, Injectable } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { NewsApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HomeDetailsPage } from '../pages/home/home';
import { HTTP } from '@ionic-native/http';
import * as xml2js from "xml2js";
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    NewsApp,
    HomePage,
    HomeDetailsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(NewsApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    NewsApp,
    HomePage,
    HomeDetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    SocialSharing,
   // Http,
   // Injectable,
    //Observable,
    //xml2js,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
