import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import * as xml2js from "xml2js";
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  templateUrl: 'home-details.html',
})
export class HomeDetailsPage {
  item;

  constructor(public params: NavParams, private socialSharing: SocialSharing, public loadingController:LoadingController) {
    this.item = params.data.item;
  }

  shareEmail(itemParam){
  	console.log("Item param is", itemParam);

  	let itemLink = 'I would like to share this article link ' + itemParam.link;
  	let itemTitle = 'Share Article - ' + itemParam.title;

  	let loading = this.loadingController.create({content : "Loading please wait..."});
    loading.present();
  	
	this.socialSharing.canShareViaEmail().then(() => {
	  
	  	this.socialSharing.shareViaEmail(itemLink, itemTitle, []).then(() => {
	  		loading.dismissAll();
		}).catch(() => {
	  	
		});
	}).catch(() => {
	 	loading.dismissAll();
	});
	
  }
}


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

public items: any; 
public addItems: any = [];
public xmlItems: any;

public news: string;

  constructor(public navCtrl: NavController, private http: HTTP, private socialSharing: SocialSharing,  public loadingController:LoadingController) {

  }

  ionViewWillEnter(){
      this.loadNewsArticles("uk");
   }

  loadNewsArticles(feed){

  	let loading = this.loadingController.create({content : "Loading please wait..."});
    loading.present();
  	//news

  	if(feed){
  	let urlFeed = "http://feeds.bbci.co.uk/news/" + feed + "/rss.xml";



	this.http.get(urlFeed, {}, {})
	  .then(data => {
	  	
	     this.parseXML(data.data)
         .then((data)=>
         {
            this.xmlItems = data;
            loading.dismissAll();

             console.log("This items are" , this.xmlItems[0].title);
             console.log("Data" , data);
         });
	   

	  })
	  .catch(error => {

	    // console.log(error.status);
	    // console.log(error.error); 

	  });
	 }
   }

   parseXML(data){
      return new Promise(resolve =>
      {
         let newsCount;
         let arrayNews = [],
             parser = new xml2js.Parser(
             {
                trim: true,
                explicitArray: true
             });

         parser.parseString(data, function (err, result)
         {
         	
            let objNews = result.rss.channel[0];
            for(newsCount in objNews.item)
            {
               let item = objNews.item[newsCount];
               arrayNews.push({
                  description  : item.description[0],
                  title        : item.title[0],
                 'media:thumbnail': item['media:thumbnail'][0],
                  link		 	: item.link[0]

               });
            }

            resolve(arrayNews);
         });
      });
   }

   onCategoryChange(selectedCategoryValue){

   	this.loadNewsArticles(selectedCategoryValue);
   }
  
  	// onProviderChange(selectedProviderValue){

   // 	this.loadNewsArticles(selectedValue);
   // }

   openSingleArticlePage(item) {

    this.navCtrl.push(HomeDetailsPage, { item: item });

  }


}
